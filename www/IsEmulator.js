var exec = require('cordova/exec');

exports.isEmulator = function (arg0, success, error) {
    exec(success, error, 'IsEmulator', 'isEmulator', [arg0]);
};
