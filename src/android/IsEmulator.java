package cordova.plugin.isemulator;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.Manifest;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.content.ContextCompat;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * This class echoes a string called from JavaScript.
 */
public class IsEmulator extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("isEmulator")) {
            Boolean message = args.getBoolean(0);
            checkWith(message, callbackContext);
            return true;
        }
        return false;
    }

    private void checkEmulator(Boolean message, CallbackContext callbackContext) {
        checkWith(message, callbackContext);
    }

    private void checkWith(boolean telephony, CallbackContext callbackContext) {
        Context context = cordova.getActivity();
        EmulatorDetector.with(context).setCheckTelephony(telephony).addPackageName("com.bluestacks")
                .addPackageName("com.google.android.launcher.layouts.genymotion")
                .addPackageName("com.bluestacks.appmart").addPackageName("com.bignox.app")
                .addPackageName("com.vphone.launcher").addPackageName("com.mumu.store").setDebug(true)
                .detect(new EmulatorDetector.OnEmulatorDetectorListener() {
                    @Override
                    public void onResult(final boolean isEmulator) {
                        JSONObject obj = new JSONObject();
                        try {
                            obj.put("isEmulator", isEmulator);
                        } catch (JSONException e) {

                        }
                        callbackContext.success(obj);
                        Log.d(getClass().getName(), "Running on emulator --> " + isEmulator);
                    }
                });
    }

    private String getCheckInfo() {
        return "\nTelephony enable is ";
        // + EmulatorDetector.with(MainActivity.this).isCheckTelephony() + "\n\n\n"
        // + EmulatorDetector.getDeviceInfo() + "\n\nEmulator Detector version " +
        // BuildConfig.VERSION_NAME;
    }
}

@TargetApi(android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
class EmulatorDetector extends CordovaPlugin {

    public interface OnEmulatorDetectorListener {
        void onResult(boolean isEmulator);
    }

    private static final String[] PHONE_NUMBERS = { "15555215554", "15555215556", "15555215558", "15555215560",
            "15555215562", "15555215564", "15555215566", "15555215568", "15555215570", "15555215572", "15555215574",
            "15555215576", "15555215578", "15555215580", "15555215582", "15555215584" };

    private static final String[] props = { "ro.build.host", "ro.product.name" };

    private static final String[] DEVICE_IDS = { "000000000000000", "e21833235b6eef10", "012345678912345" };

    private static final String[] IMSI_IDS = { "310260000000000" };

    private static final String[] GENY_FILES = { "/dev/socket/genyd", "/dev/socket/baseband_genyd" };

    private static final String[] QEMU_DRIVERS = { "goldfish" };

    private static final String[] PIPES = { "/dev/socket/qemud", "/dev/qemu_pipe" };

    private static final String[] X86_FILES = { "ueventd.android_x86.rc", "x86.prop", "ueventd.ttVM_x86.rc",
            "init.ttVM_x86.rc", "fstab.ttVM_x86", "fstab.vbox86", "init.vbox86.rc", "ueventd.vbox86.rc",
            "/boot/bstsetup.env", "/system/lib/libc_malloc_debug_qemu.so", "/sys/qemu_trace", "/system/bin/qemu-props",
            "/system/bin/bstshutdown" };

    private static String[] known_bluestacks = { "/data/app/com.bluestacks.appmart-1.apk",
            "/data/app/com.bluestacks.BstCommandProcessor-1.apk", "/data/app/com.bluestacks.help-1.apk",
            "/data/app/com.bluestacks.home-1.apk", "/data/app/com.bluestacks.s2p-1.apk",
            "/data/app/com.bluestacks.searchapp-1.apk", "/data/bluestacks.prop", "/data/data/com.androVM.vmconfig",
            "/data/data/com.bluestacks.accelerometerui", "/data/data/com.bluestacks.appfinder",
            "/data/data/com.bluestacks.appmart", "/data/data/com.bluestacks.appsettings",
            "/data/data/com.bluestacks.BstCommandProcessor", "/data/data/com.bluestacks.bstfolder",
            "/data/data/com.bluestacks.help", "/storage/emulated/0/Android/data/com.bluestacks.home",
            "/sdcard/Android/data/com.bluestacks.home", "/data/data/com.bluestacks.s2p",
            "/data/data/com.bluestacks.searchapp", "/storage/emulated/0/Android/data/com.bluestacks.settings",
            "/sdcard/Android/data/com.bluestacks.settings",
            "/storage/emulated/0/Android/data/com.bluestacks.gamepophome", "/data/data/com.bluestacks.setup",
            "/data/data/com.bluestacks.spotlight", "/mnt/prebundledapps/bluestacks.prop.orig" };

    private static final String[] ANDY_FILES = { "fstab.andy", "ueventd.andy.rc" };

    private static final String[] NOX_FILES = { "fstab.nox", "init.nox.rc", "ueventd.nox.rc" };

    private static final Property[] PROPERTIES = { new Property("init.svc.qemud", null),
            new Property("init.svc.qemu-props", null), new Property("qemu.hw.mainkeys", null),
            new Property("qemu.sf.fake_camera", null), new Property("qemu.sf.lcd_density", null),
            new Property("ro.bootloader", "unknown"), new Property("ro.bootmode", "unknown"),
            new Property("ro.hardware", "goldfish"), new Property("ro.kernel.android.qemud", null),
            new Property("ro.kernel.qemu.gles", null), new Property("ro.kernel.qemu", "1"),
            new Property("ro.product.device", "generic"), new Property("ro.product.model", "sdk"),
            new Property("ro.product.name", "sdk"), new Property("ro.serialno", null) };

    private static final String IP = "10.0.2.15";

    private static final int MIN_PROPERTIES_THRESHOLD = 0x5;

    private final Context mContext;
    private boolean isDebug = false;
    private boolean isTelephony = false;
    private boolean isCheckPackage = true;
    private List<String> mListPackageName = new ArrayList<>();

    @SuppressLint("StaticFieldLeak") // Since we use application context now this won't leak memory anymore. This is
                                     // only to please Lint
    private static EmulatorDetector mEmulatorDetector;

    public static EmulatorDetector with(Context pContext) {
        if (pContext == null) {
            throw new IllegalArgumentException("Context must not be null.");
        }
        if (mEmulatorDetector == null)
            mEmulatorDetector = new EmulatorDetector(pContext.getApplicationContext());
        return mEmulatorDetector;
    }

    private EmulatorDetector(Context pContext) {
        mContext = pContext;
        mListPackageName.add("com.google.android.launcher.layouts.genymotion");
        mListPackageName.add("com.bluestacks");
        mListPackageName.add("com.bignox.app");
        mListPackageName.add("com.vphone.launcher");
        mListPackageName.add("com.bignox.app");
    }

    public EmulatorDetector setDebug(boolean isDebug) {
        this.isDebug = isDebug;
        return this;
    }

    public boolean isDebug() {
        return isDebug;
    }

    public boolean isCheckTelephony() {
        return isTelephony;
    }

    public boolean isCheckPackage() {
        return isCheckPackage;
    }

    public EmulatorDetector setCheckTelephony(boolean telephony) {
        this.isTelephony = telephony;
        return this;
    }

    public EmulatorDetector setCheckPackage(boolean chkPackage) {
        this.isCheckPackage = chkPackage;
        return this;
    }

    public EmulatorDetector addPackageName(String pPackageName) {
        this.mListPackageName.add(pPackageName);
        return this;
    }

    public EmulatorDetector addPackageName(List<String> pListPackageName) {
        this.mListPackageName.addAll(pListPackageName);
        return this;
    }

    public List<String> getPackageNameList() {
        return this.mListPackageName;
    }

    public void detect(final OnEmulatorDetectorListener pOnEmulatorDetectorListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean isEmulator = detect();
                log("This System is Emulator: " + isEmulator);
                if (pOnEmulatorDetectorListener != null) {
                    pOnEmulatorDetectorListener.onResult(isEmulator);
                }
            }
        }).start();
    }

    private boolean detect() {
        boolean result = false;

        log(getDeviceInfo());

        // Check Basic
        if (!result) {
            result = checkBasic();
            log("Check basic " + result);
        }

        // Check Advanced
        if (!result) {
            result = checkAdvanced();
            log("Check Advanced " + result);
        }

        // Check Package Name
        if (!result) {
            result = checkPackageName();
            log("Check Package Name " + result);
        }

        // Check System Prop
        if (!result) {
            result = checkSystemProp();
            log("Check System Prop " + result);
        }

        // Check CPU Info
        if (!result) {
            result = checkCpuInfo();
            log("Check CPU Info " + result);
        }

        // Check Bluestacks Files
        if (!result) {
            result = checkBlueStacksFiles();
            log("Check Bluestacks Files " + result);
        }

        return result;
    }

    private boolean checkBasic() {
        boolean result = android.os.Build.FINGERPRINT.startsWith("generic")
                || android.os.Build.MODEL.contains("google_sdk")
                || android.os.Build.MODEL.toLowerCase().contains("droid4x")
                || android.os.Build.MODEL.contains("Emulator")
                || android.os.Build.MODEL.contains("Android SDK built for x86")
                || android.os.Build.MANUFACTURER.contains("Genymotion") || android.os.Build.HARDWARE.equals("goldfish")
                || android.os.Build.HARDWARE.equals("vbox86") || android.os.Build.PRODUCT.equals("sdk")
                || android.os.Build.PRODUCT.equals("google_sdk") || android.os.Build.PRODUCT.equals("sdk_x86")
                || android.os.Build.PRODUCT.equals("vbox86p") || android.os.Build.BOARD.toLowerCase().contains("nox")
                || android.os.Build.BOOTLOADER.toLowerCase().contains("nox")
                || android.os.Build.HARDWARE.toLowerCase().contains("nox")
                || android.os.Build.PRODUCT.toLowerCase().contains("nox")
                || android.os.Build.SERIAL.toLowerCase().contains("nox");

        if (result)
            return true;
        result |= android.os.Build.BRAND.startsWith("generic") && android.os.Build.DEVICE.startsWith("generic");
        if (result)
            return true;
        result |= "google_sdk".equals(android.os.Build.PRODUCT);
        return result;
    }

    private boolean checkAdvanced() {
        log("checkQEmu::" + checkQEmuProps());
        log("checkFiles::" + checkFiles(X86_FILES, "X86"));
        boolean result = checkTelephony() || checkFiles(GENY_FILES, "Geny") || checkFiles(ANDY_FILES, "Andy")
                || checkFiles(NOX_FILES, "Nox") || checkQEmuDrivers() || checkFiles(PIPES, "Pipes") || checkIp()
                || (checkQEmuProps() && checkFiles(X86_FILES, "X86"));
        return result;
    }

    private boolean checkPackageName() {
        if (!isCheckPackage || mListPackageName.isEmpty()) {
            return false;
        }
        // Context ctx = mContext.getActivity().getApplicationContext();
        final PackageManager pm = mContext.getPackageManager();
        for (final String pkgName : mListPackageName) {
            try {
                pm.getPackageInfo(pkgName, PackageManager.GET_ACTIVITIES);
                return true;
            } catch (PackageManager.NameNotFoundException e) {
                return false;
            }
        }
        return false;
    }

    private boolean checkSystemProp() {

        for (String prop : props) {
            try {

                Process p = Runtime.getRuntime().exec("getprop " + prop);

                BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));

                String l = b.readLine();
                log("SYSTEM PROP::" + l);
                if (l.equals("buildbot.soft.genymobile.com")) {

                }

                Log.d("Exec Detect", l);

                p.destroy();

            } catch (IOException e) {

                e.printStackTrace();

            }
        }
        return false;
    }

    public boolean checkCpuInfo() {
        String cpuInfo = readCpuInfo();
        log("CPU INFO::" + cpuInfo);
        if ((cpuInfo.contains("intel") || cpuInfo.contains("amd"))) {
            return true;
        }

        return false;
    }

    public static String readCpuInfo() {
        String result = "";
        try {
            String[] args = { "/system/bin/cat", "/proc/cpuinfo" };
            ProcessBuilder cmd = new ProcessBuilder(args);

            Process process = cmd.start();
            StringBuffer sb = new StringBuffer();
            String readLine = "";
            BufferedReader responseReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream(), "utf-8"));
            while ((readLine = responseReader.readLine()) != null) {
                sb.append(readLine);
            }
            responseReader.close();
            result = sb.toString().toLowerCase();
        } catch (IOException ex) {
        }
        return result;
    }

    public boolean checkBlueStacksFiles() {
        for (int i = 0; i < known_bluestacks.length; i++) {
            String file_name = known_bluestacks[i];
            File qemu_file = new File(file_name);
            if (qemu_file.exists()) {
                log("Bluestack found::" + qemu_file);
                return true;
            }
        }
        log("Result : Not Find BlueStacks Files!");
        return false;
    }

    private boolean checkTelephony() {
        log("IS TELEPHONY::" + this.isTelephony);
        log("IS PERMISSIN GRANTED::" + (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED));
        log("IS SUPPORT TELE::" + isSupportTelePhony());
        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED && this.isTelephony
                && isSupportTelePhony()) {
            return checkPhoneNumber() || checkDeviceId() || checkImsi() || checkOperatorNameAndroid();
        }
        return false;
    }

    private boolean checkPhoneNumber() {
        TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            @SuppressLint({ "HardwareIds", "MissingPermission" })
            String phoneNumber = telephonyManager.getLine1Number();
            for (String number : PHONE_NUMBERS) {
                if (number.equalsIgnoreCase(phoneNumber)) {
                    log("Check phone number is detected");
                    return true;
                }
            }
        } catch (Exception e) {
            log("No permission to detect access of Line1Number");
        }
        return false;
    }

    private boolean checkDeviceId() {
        TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            @SuppressLint({ "HardwareIds", "MissingPermission" })
            String deviceId = telephonyManager.getDeviceId();
            log("DEVICE ID::" + deviceId);
            for (String known_deviceId : DEVICE_IDS) {
                if (known_deviceId.equalsIgnoreCase(deviceId)) {
                    log("Check device id is detected");
                    return true;
                }

            }
        } catch (Exception e) {
            log("No permission to detect access of DeviceId");
        }
        return false;
    }

    private boolean checkImsi() {
        TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);

        try {
            @SuppressLint({ "HardwareIds", "MissingPermission" })
            String imsi = telephonyManager.getSubscriberId();
            log("imsi ID::" + imsi);
            for (String known_imsi : IMSI_IDS) {
                if (known_imsi.equalsIgnoreCase(imsi)) {
                    log("Check imsi is detected");
                    return true;
                }
            }
        } catch (Exception e) {
            log("No permission to detect access of SubscriberId");
        }
        return false;
    }

    private boolean checkOperatorNameAndroid() {
        String operatorName = ((TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE))
                .getNetworkOperatorName();
        log("operator ID::" + operatorName);
        if (operatorName.equalsIgnoreCase("android")) {
            log("Check operator name android is detected");
            return true;
        }
        return false;
    }

    private boolean checkQEmuDrivers() {
        for (File drivers_file : new File[] { new File("/proc/tty/drivers"), new File("/proc/cpuinfo") }) {
            if (drivers_file.exists() && drivers_file.canRead()) {
                byte[] data = new byte[1024];
                try {
                    InputStream is = new FileInputStream(drivers_file);
                    is.read(data);
                    is.close();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }

                String driver_data = new String(data);
                for (String known_qemu_driver : QEMU_DRIVERS) {
                    if (driver_data.contains(known_qemu_driver)) {
                        log("Check QEmuDrivers is detected");
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private boolean checkFiles(String[] targets, String type) {
        for (String pipe : targets) {
            File qemu_file = new File(pipe);
            if (qemu_file.exists()) {
                log("Check " + type + " is detected");
                return true;
            }
        }
        return false;
    }

    private boolean checkQEmuProps() {
        int found_props = 0;

        for (Property property : PROPERTIES) {
            String property_value = getProp(mContext, property.name);
            if ((property.seek_value == null) && (property_value != null)) {
                found_props++;
            }
            if ((property.seek_value != null) && (property_value.contains(property.seek_value))) {
                found_props++;
            }

        }

        if (found_props >= MIN_PROPERTIES_THRESHOLD) {
            log("Check QEmuProps is detected");
            return true;
        }
        return false;
    }

    private boolean checkIp() {
        boolean ipDetected = false;
        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED) {
            String[] args = { "/system/bin/netcfg" };
            StringBuilder stringBuilder = new StringBuilder();
            try {
                ProcessBuilder builder = new ProcessBuilder(args);
                builder.directory(new File("/system/bin/"));
                builder.redirectErrorStream(true);
                Process process = builder.start();
                InputStream in = process.getInputStream();
                byte[] re = new byte[1024];
                while (in.read(re) != -1) {
                    stringBuilder.append(new String(re));
                }
                in.close();

            } catch (Exception ex) {
                // empty catch
            }

            String netData = stringBuilder.toString();
            log("netcfg data -> " + netData);

            if (!TextUtils.isEmpty(netData)) {
                String[] array = netData.split("\n");

                for (String lan : array) {
                    if ((lan.contains("wlan0") || lan.contains("tunl0") || lan.contains("eth0")) && lan.contains(IP)) {
                        ipDetected = true;
                        log("Check IP is detected");
                        break;
                    }
                }

            }
        }
        return ipDetected;
    }

    private String getProp(Context context, String property) {
        try {
            ClassLoader classLoader = context.getClassLoader();
            Class<?> systemProperties = classLoader.loadClass("android.os.SystemProperties");

            Method get = systemProperties.getMethod("get", String.class);

            Object[] params = new Object[1];
            params[0] = property;

            return (String) get.invoke(systemProperties, params);
        } catch (Exception exception) {
            // empty catch
        }
        return null;
    }

    private boolean isSupportTelePhony() {
        PackageManager packageManager = mContext.getPackageManager();
        boolean isSupport = packageManager.hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
        log("Supported TelePhony: " + isSupport);
        return isSupport;
    }

    private void log(String str) {
        if (this.isDebug) {
            Log.d(getClass().getName(), str);
        }
    }

    public static String getDeviceInfo() {
        return "Build.PRODUCT: " + android.os.Build.PRODUCT + "\n" + "Build.MANUFACTURER: "
                + android.os.Build.MANUFACTURER + "\n" + "Build.BRAND: " + android.os.Build.BRAND + "\n"
                + "Build.DEVICE: " + android.os.Build.DEVICE + "\n" + "Build.MODEL: " + android.os.Build.MODEL + "\n"
                + "Build.HARDWARE: " + android.os.Build.HARDWARE + "\n" + "Build.FINGERPRINT: "
                + android.os.Build.FINGERPRINT;
    }
}

class Property {
    public String name;
    public String seek_value;

    public Property(String name, String seek_value) {
        this.name = name;
        this.seek_value = seek_value;
    }
}
